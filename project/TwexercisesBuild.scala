import sbt._
import sbt.Keys._

object TwexercisesBuild extends Build {

  lazy val twexercises = Project(
    id = "twexercises",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "twexercises",
      organization := "com.njk",
      version := "0.1-SNAPSHOT",
      scalaVersion := "2.11.0"
      // add other settings here
    )
  )
}
