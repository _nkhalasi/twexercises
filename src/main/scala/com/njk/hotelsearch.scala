package com.njk

import java.util.{Calendar, Date}

trait DateUtils {
	implicit def str2date(dateStr: String) = {
		import java.text.SimpleDateFormat
		val format = new SimpleDateFormat("dd-MM-yyyy")
		format.parse(dateStr)
	}

	val WEEKENDS = List(Calendar.FRIDAY, Calendar.SATURDAY, Calendar.SUNDAY)
	def isWeekend(d: Date) = {
		val cal = Calendar.getInstance()
		cal.setTime(d)
		if (WEEKENDS.exists(cal.get(Calendar.DAY_OF_WEEK) == _)) true else false
	}

	def isWeekday(d: Date) = if (isWeekend(d)) false else true

	def date(d: Date) = d
}

case class Hotel(name: String, rating: Int)

trait HotelDB extends DateUtils {
	val hotels = Map(
			Hotel("Lakewood", 3) -> Map("regular::weekday" -> 110, "reward::weekday" -> 80, "regular::weekend" -> 90, "reward::weekend" -> 80),
			Hotel("Bridgewood", 4) -> Map("regular::weekday" -> 160, "reward::weekday" -> 110, "regular::weekend" -> 60, "reward::weekend" -> 50),
			Hotel("Riverwood", 5) -> Map("regular::weekday" -> 220, "reward::weekday" -> 100, "regular::weekend" -> 150, "reward::weekend" -> 40)
		)

	import scala.collection.mutable.{Map => MutableMap}
	def findCheapest(customerType: String, dates: Date*) = {
		val dayTypes = dates.map(d => if (isWeekend(d)) "weekend" else "weekday")
		val dayTypesEnriched = dayTypes.map(customerType+"::"+_)
		val hotelRates = MutableMap.empty[Hotel, Double]
		for ((hotel, rates) <- hotels) {
			hotelRates(hotel) = dayTypesEnriched.map(rates(_)).foldLeft(0.0)(_ + _)
		}
		hotelRates.minBy(_._2)._1
	}
}

object HotelSearch extends App with HotelDB {
	println(findCheapest("regular", date("14-7-2014"), date("13-7-2014"), date("12-7-2014"), date("11-7-2014"), date("10-7-2014"))) //Lakewood
	println(findCheapest("reward", date("14-7-2014"), date("13-7-2014"), date("12-7-2014"), date("11-7-2014"), date("10-7-2014"))) //Riverwood

	println(findCheapest("regular", date("16-3-2009"), date("17-03-2009"), date("18-03-2009"))) //Lakewood
	println(findCheapest("regular", date("20-3-2009"), date("21-03-2009"), date("22-03-2009"))) //Bridgewood
	println(findCheapest("reward", date("26-3-2009"), date("27-03-2009"), date("28-03-2009"))) //Riverwood
}