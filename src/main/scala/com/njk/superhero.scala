package com.njk

trait PhoneKeypadMap {
    val numToAlpha = Map(2 -> "ABC", 3 -> "DEF", 4 -> "GHI", 5 -> "JKL", 6 -> "MNO", 7 -> "PQRS", 8 -> "TUV", 9 -> "WXYZ")
    val alphaToNum = for {
                (n, cs) <- numToAlpha
                c <- cs
            } yield (c, n)
}

trait SuperHeroes extends PhoneKeypadMap {
    val superheroNames = List("SUPERMAN", "THOR", "ROBIN", "IRONMAN", "GHOSTRIDER", "CAPTAINAMERICA", "FLASH", 
        "WOLVERINE", "BATMAN", "HULK", "BLADE", "PHANTOM", "SPIDERMAN", "BLACKWIDOW", "HELLBOY", "PUNISHER")
    val codeToName = (for {
            name <- superheroNames
        } yield (name.map(c => alphaToNum(c)).mkString, name)).toMap
}

object SuperHeroConverter extends App with SuperHeroes {
    def printStressSignal(hero: Option[String]) = hero match {
        case Some(name) => println(s"Send distress signal to ${name}")
        case None => println(s"Invalid hero requested")
    }

    def parseArg(arg: String) = arg.toSeq match {
        case Seq('0', ' ', code@_*) => code.mkString
        case _ => "Invalid"
    }

    println(codeToName)
    val heros = args.map(parseArg(_))
    println(heros) 
    heros.foreach(h => printStressSignal(codeToName.get(h)))
}